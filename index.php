<!DOCTYPE html>
<html lang="es">
<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="shortcut icon" href="img/bl.png">

  <!--Boostrap-css-->
  <link rel="stylesheet" href="https://beacon-link.com/wp-content/themes/beacon-theme/css/animation-aos.css">
  <link href="https://beacon-link.com/wp-content/themes/beacon-theme/css/aos.css" rel="stylesheet prefetch" type="text/css" media="all">
  <!-- Animation-->
  <link rel="stylesheet" href="https://beacon-link.com/wp-content/themes/beacon-theme/css/style.css" type="text/css" media="all">
  <!--Style css-->
  <link rel="stylesheet" href="https://beacon-link.com/wp-content/themes/beacon-theme/css/font-awesome.css">
  <link rel="stylesheet" href="https://beacon-link.com/wp-content/themes/beacon-theme/style.css" type="text/css" media="all">
  <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
  <title>Beacon Link</title>
  <!-- Font Awesome Icons-->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <!-- Google Fonts-->
  <link rel="dns-prefetch" href="//www.google.com">
  <link href="https://fonts.googleapis.com/css?family=Merriweather+Sans:400,700" rel="stylesheet">
  <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic' rel='stylesheet' type='text/css'>

  <!-- Plugin CSS-->
  <link href="vendor/magnific-popup/magnific-popup.css" rel="stylesheet">

  <!-- Theme CSS - Includes Bootstrap-->
  <link href="scss/_navbar.scss" rel="stylesheet" >
  <link href="assets/css/style.css" rel="stylesheet">
  <link href="css/creative.min.css" rel="stylesheet"> 


<!--Video modal-->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

<script src="js/scripts.js"></script>

<!--Carousel-->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
  <style>
  /* Make the image fully responsive */
  .carousel-inner img {
      width: 100%;
      height: 100%;
  }
  </style>
		



</head>

<body id="page-top">

  <!--Preloader-->
  <div class="preloader">
    <div class="inner">
        <div class="item item1"></div>
        <div class="item item2"></div>
        <div class="item item3"></div>
    </div>
    <script type="text/javascript" src="js/lib/jquery-1.11.2.min.js"></script>
</div>

  <!-- Navigation -->
     <nav  class="navbar navbar-expand-lg navbar-light fixed-top py-3" id="mainNav">
    <div class="container">
      <a class="navbar-brand"  href="#page-top"><img src="https://beacon-link.com/wp-content/themes/beacon-theme/images/logo.png" class="logo"></a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto my-2 my-lg-0 align-content-end" >
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger"  href="#page-top">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger"  href="#aboutus">About us</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" data-toggle="modal" data-target="#myModal" >Government</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="#training">Training</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="#services">Services</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="#client">Clients</a>
          </li>
           <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="#findus">Find us</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="contacus.html">Contact us</a>
          </li>
          
        </ul>
      </div>
    </div>
  </nav>

  <!-- Masthead -->
<header class="masthead">
 <div class="container h-100">
      <div class="row h-100 align-items-center justify-content-center text-center">
        <div class="col-lg-10 align-self-end">
          <h1 class="text-uppercase text-white font-weight-bold">Conveying meanings</h1>
          <hr class="divider my-4">
        </div>
        <div class="col-lg-8 align-self-baseline">
          <p class="text-white-75 font-weight-light mb-5">Communicational Solutions in 120+ languages and more</p>
          
        </div>
      </div>
    </div>
</header>

  <!-- About Section -->
 <section class="page-section bg-dark"  id="about" style="background-color: lightblue;">
    <div class="video-promo" style="text-align: center;">
      <a class="youtube cboxElement" href="https://www.youtube.com/embed/gZwRwWx91w4?rel=0&amp;wmode=transparent" title="View video">
        <img src="">
      </a>
    </div>
 </section> 
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>

<!--Section About-->

<div>

    <div class="main-w3pvt">
      <div class="container-fluid">
        <div class="row">
          <div class="container py-xl-5 py-lg-3">
            <!-- About us Section -->
            <section  id="aboutus">
                <div class="page-section mt-0 col-md-12 row ">
             <!-- Lado Izquierdo -->
                    <div class="izq col-md-6 text-justify mt-4 ">
                      <div style="visibility: visible; opacity: 1; transform: matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transition: opacity 1s cubic-bezier(0.5, 0, 0, 1) 0s, transform 1s cubic-bezier(0.5, 0, 0, 1) 0s;">
                        <h1 class="text-justify text-white text-center">About us</h1>
                        <p class="text-justify text-white">Beacon Link manages mechanisms of intercultural communication, providing educational, marketing and media solutions to Individuals, Corporate and Government clients at a Local, Nationwide and International scale facilitated by certified professionals delivering the power of language and media to promote expansion and worldwide connections.</p>
          <p class="text-justify text-white">This company is a beacon for language, cultural, and a commercial link between diverse parties to enhance global development.</p>
          <p class="text-justify text-white" >Beacon Link is a national and international reference for marketing, education, interpretation and translation services; and we generate fidelity with our clients through branding and marketing structures.</p>
                      </div>
                    </div>
            <!-- Lado Derecho -->
                    <div class="der col-lg-6 col-md-6 mt-4  container">
                      <h1 class="text-justify text-white text-center">Our Team</h1>
                      <img src="img/alfredo-herrera.png" alt="" width=" 330px" height="200px">
                      <br>
                      <br>
                      <h4 class="text-justify text-white text-center">José Alfredo Herrera</h4>
                      <p class="text-justify text-white-75 text-center">CEO</p>
                      <p class="text-justify text-white">
                        Over 20 years of experience in the Interpretation and Translation Field. 
                        Licensed and Principal Instructor of “The Beacon Interpreter Training”</p>
                    </div>
                </div>
            </section>
            </div>          
        <ul class="bg-bubbles">
          <li></li>
          <li></li>
          <li></li>
          <li></li>
          <li></li>
          <li></li>
          <li></li>
          <li></li>
          <li></li>
          <li></li>
        </ul>
      </div>
    </div>
    </div>
  
<!-- fin About-->

<!-- Video de presentacion-->

  <div class="video-promo" style="text-align: center;">
		  
    <br>
  <br>
    <button class="btn btn-link btn-lg video" data-video="https://www.youtube.com/embed/gZwRwWx91w4?rel=0&amp;wmode=transparent" data-toggle="modal" data-target="#videoModal"><img src="https://beacon-link.com/wp-content/uploads/2018/11/play-button.png"></button>
  <div class="modal fade" id="videoModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-body">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <iframe width="150%" height="350" src="" frameborder="0" requestfullscreen></iframe>
      </div>
    </div>
    </div>
  </div>
  </div>

<!-- Fin about us-->

 <!--Texto de Training-->       
 <div id="training" class="introducing py-5">
    <h1 class="left-text" data-sr-id="5" style="visibility: visible; opacity: 1; transform: matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transition: opacity 1s cubic-bezier(0.5, 0, 0, 1) 0s, transform 1s cubic-bezier(0.5, 0, 0, 1) 0s;">INTRODUCING</h1>
    <h2 class="left-right" data-sr-id="6" style="visibility: visible; opacity: 1; transform: matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transition: opacity 1s cubic-bezier(0.5, 0, 0, 1) 0s, transform 1s cubic-bezier(0.5, 0, 0, 1) 0s;">The Beacon Interpreter Training</h2>
  </div>

 

 <!-- Training carousel-->
 
    <div class="training" id="training">
      <div id="demo" class="carousel slide" data-ride="carousel">

        <!-- Indicators -->
        <ul class="carousel-indicators">
          <li data-target="#demo" data-slide-to="0" class="active"></li>
          <li data-target="#demo" data-slide-to="1"></li>
          <li data-target="#demo" data-slide-to="2"></li>
        </ul>
      
        <!-- The slideshow -->
        <div class="carousel-inner">
          <div class="carousel-item active">
            <img style="height: 550px;" src="img/c1.jpeg">
            <div class="carousel-caption"> 
              <h1 style="text-align: left; color: yellow;">Who should attend</h1>
        <ul style="text-align: left; font-size: 25px;">
           <li>
             Bilingual staff experienced in interpreting for clients
          </li>
           <li>
             Individuals interested in becoming Professional Interpreters
          </li>
           <li>
             Individual interpreters who have never taken interpretation training
          </li>
           <li>
             Interpreters interested in becoming	certified
          </li>
        </ul>
        &nbsp;
        <p style="font-size: 25px; text-align: center;"><a class="btn btn-primary btn-warning" href="checkout.php" tabindex="-1" style="color:black;">ENROLL NOW!</a></p>
            </div>
          </div>
          <div class="slideInd slick-slide" style="background-image: url(&quot;https://beacon-link.com/wp-content/uploads/2018/11/Banner-1.png&quot;); width: 424px;" data-slick-index="2" aria-hidden="true" tabindex="-1" role="option" aria-describedby="0"></div>



          <div class="carousel-item">
            <img style="height: 550px;" src="img/c2.jpeg">
            <div class="carousel-caption"> 
              <h2 style="text-align: center; color: yellow;"><strong>TRAININGS COMING UP</strong></h2>
        <h2 style="text-align: center;">MODESTO, CA -&nbsp; March 14 - April 11,&nbsp; 2020</h2>
        <h2 style="text-align: center;">ATLANTA, GA - April 18 - May 16,&nbsp; 2020</h2>
        &nbsp;
        <p style="font-size: 25px; "><a class="btn btn-primary btn-warning" href="checkout.php" tabindex="-1" style="color:black;">ENROLL NOW!</a></p>
        
        </div>
          </div>




          <div class="carousel-item">
            <img style="height: 550px;" src="img/c3.jpeg">

            <div class="carousel-caption" style="font-size: 15px; text-align: left;"> 
              <h1 style="color: yellow; ">COURSE TOPICS</h1>
        <ul>
          <li>Introduction to Community Interpreting</li>
          <li>Interpretation Protocols</li>
          <li>CLAS Standards, Title VI and HIPPA</li>
          <li>Strategic Mediation</li>
          <li>Professional Identity</li>
          <li>The Role of the interpreter</li>
          <li>Interpreter Role Play with Language Coach</li>
          <li>Strategies for Communicating
        and Connecting Across Cultures</li>
          <li>How to Work with a Professionally
        Trained Interpreter and Untrained Bilingual Staff</li>
        </ul>
        &nbsp;
        <p class="button" style="font-size: 25px; text-align: center;"><a class="btn btn-primary btn-warning" href="checkout.php" tabindex="-1" style="color:black;">ENROLL NOW!</a></p>
            </div>
          </div>
        </div>
      
        <!-- Left and right controls -->
        <a class="carousel-control-prev" href="#demo" data-slide="prev">
          <span class="carousel-control-prev-icon"></span>
        </a>
        <a class="carousel-control-next" href="#demo" data-slide="next">
          <span class="carousel-control-next-icon"></span>
        </a>
      
      </div>
      



  

 



 <!--Seccion de Suscripcion
  <section class="page-section bg-white text-white">
    <div >
       formulario de suscripcion
      <div class="col-md-16" style="background-color: white;">
        <div class="container py-xl-5 py-lg-3">
          <h3 class="tittle text-center text-white font-weight-bold">Subscribe to The Beacon Newsletter</h3>
          <p class="sub-tittle text-center mt-3 mb-sm-5 mb-4">Get updates and great offers on Beacon Link services</p>
          <div class="n-right-w3ls">
            <div role="form" class="wpcf7" id="wpcf7-f145-o1" lang="en-US" dir="ltr">
  <div class="screen-reader-response"></div>
  Formulario de la suscripcion
     <form action="/#wpcf7-f145-o1" method="post" class="wpcf7-form mailchimp-ext-0.4.50" novalidate="novalidate">
        <div style="display: none;">
         <input type="hidden" name="_wpcf7" value="145">
         <input type="hidden" name="_wpcf7_version" value="5.1.1">
         <input type="hidden" name="_wpcf7_locale" value="en_US">
         <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f145-o1">
         <input type="hidden" name="_wpcf7_container_post" value="0">
         <input type="hidden" name="g-recaptcha-response" value="03AERD8XrlequX_A5M8PhKZ4BSbv0txP13DDPvs6fFoucIZDCC4M6gwFUF-N_jDANaRMkV8sZrKkpww5t061oOxrIeC8LDrZ7c7MMhK0rMCLs19W1ITVsMcV8e6hxZNdFm7-LQ9bnEZqZSsMGRitUDG9l9BItD9yXICRFeKGFSui37uunBfFFO7p5BZ-Vhc_l2Ue0_YSO9RjhRWhYbjBZ5hCqx2-Ab5zg1mZUb9fMyOajnYb3JfXYNpR93DDvsyfXjSZdALmYE4zGiOC6-wPKsiRH2v6R37IPimsF0CKWzmA8KvFNhxuL4QX4_2x1N3OdStrtftTVoAyIMrET6x0uy9m0B0no11PdBTZyRpdNFP3NUi5wRtN91h4bbI0bTDYHu_0e7I5_tC0sH">
        </div>
     <p><span class="wpcf7-form-control-wrap your-name"><input type="text" name="your-name" value="" size="60" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="Name"></span></p>
     <p><span class="wpcf7-form-control-wrap your-email"><input type="email" name="your-email" value="" size="60" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" placeholder="Email Address"></span></p>
     <p><input type="submit" value="Subscribe" class="wpcf7-form-control wpcf7-submit" ><span class="ajax-loader"></span></p>
    <div class="wpcf7-response-output wpcf7-display-none"></div><p style="display: none !important"><span class="wpcf7-form-control-wrap referer-page"><input type="hidden" name="referer-page" value="https://beacon-link.com/interpretation/" class="wpcf7-form-control wpcf7-text referer-page" aria-invalid="false"></span></p>
   </form>Fin de formulario
  </div>				
  </div>
  </div>
</div>  
  </section>-->
  <!--Fin de seccion de Suscripcion -->


 <!-- Services Section -->
 <section class="page-section" id="services">
    <div class="container">
      <h2 class="text-center mt-0">Our Services</h2>
      <hr class="divider my-4">
      <div class="row">
        <div class="col-lg-3 col-md-6 text-center">
          <div class="mt-5">
            <i class="fas fa-4x fa-home text-primary" style="color: #339af0;"></i>
            <h3 class="h4 mb-2">IN HOUSE INTERPRETERS</h3>
            <p class="text-muted mb-0">All Languages Nationwide</p>
          </div>
        </div>
        <div class="col-lg-3 col-md-6 text-center">
          <div class="mt-5">
            <i class="fas fa-4x fa-users text-primary mb-4"></i>
            <h3 class="h4 mb-2">On-SITE INTERPRETATION</h3>
            <p class="text-muted mb-0">All Languages Nationwide</p>
          </div>
        </div>
        <div class="col-lg-3 col-md-6 text-center">
          <div class="mt-5">
            <i class="fas fa-4x fa-headset text-primary mb-4"></i>
            <h3 class="h4 mb-2">VIDEO REMOTE INTERPRETATION(VRI)</h3>
            <p class="text-muted mb-0">Spanish 24/7, all languages including ASL pre-scheduled.</p>
          </div>
        </div>
        <div class="col-lg-3 col-md-6 text-center">
          <div class="mt-5">
            <i class="fas fa-4x fa-file-alt text-primary mb-4" style="color: #339af0;"></i>
            <h3 class="h4 mb-2">DOCUMENT TRANSLATION</h3>
            <p class="text-muted mb-0">All Languages</p>
          </div>
        </div>
      </div>
    </div>
  </section>



  <!-- Modal goverment-->

      <div class="container">
        
        <!-- The Modal -->
        <div class="modal fade" id="myModal">
          <div class="modal-dialog modal-dialog modal-lg">
            <div class="modal-content">
            
              <!-- Modal Header -->
              <div class="modal-header">
                <h4 class="modal-title">Government</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
              </div>
              
              <!-- Modal body -->
              <div class="modal-body">
                <object class="pdfdoc" width="750px" height="450px" type="application/pdf" data="info/Capability Statement PDF 2.pdf"></object>
              </div>
              
              <!-- Modal footer -->
              
              
            </div>
          </div>
        </div>
        
      </div>

         <!-- Portfolio Section -->
  		<!-- contact -->


      <div class="main-w3pvt">
      <div class="container-fluid">


      <div class="client py-5" id="client" >
        <div class="container py-xl-5 py-lg-3">
          <h3 class=" text-center text-white font-weight">BUSINESSES TRUST BEACON LINK</h3>
          <div class="row inner-sec-w3layouts-w3pvt-lauinfo">
                        <div class="col-sm-3 team-grids text-center">
                  <img src="https://beacon-link.com/wp-content/uploads/2019/03/client3.jpg" class="img-fluid" alt="" />
                  <div class="team-info">
                    <div class="caption">
                      <h4>CARTERSVILLE MEDICAL</h4>
                    </div>
                  </div>
                </div>
                            <div class="col-md-3 team-grids text-center">
                  <img src="https://beacon-link.com/wp-content/uploads/2019/03/client2.jpg" class="img-fluid" alt="" />
                  <div class="team-info">
                    <div class="caption">
                      <h4>CARTERSVILLE CITY SCHOOLS</h4>
                    </div>
                  </div>
                </div>
                            <div class="col-md-3 team-grids text-center">
                  <img src="https://beacon-link.com/wp-content/uploads/2019/03/cherokee-county-school.jpg" class="img-fluid" alt="" />
                  <div class="team-info">
                    <div class="caption">
                      <h4>CHEROKEE COUNTY SCHOOL</h4>
                    </div>
                  </div>
                </div>
                            <div class="col-md-3 team-grids text-center">
                  <img src="https://beacon-link.com/wp-content/uploads/2019/03/gordon-county-school.jpg" class="img-fluid" alt="" />
                  <div class="team-info">
                    <div class="caption">
                      <h4>GORDON COUNTY SCHOOL</h4>
                    </div>
                  </div>
                </div>
                            <div class="col-md-3 team-grids text-center">
                  <img src="https://beacon-link.com/wp-content/uploads/2019/03/perrottalaw_logo.jpg" class="img-fluid" alt="" />
                  <div class="team-info">
                    <div class="caption">
                      <h4>PERROTTA, LAMB &#038; JOHNSON, LLC.</h4>
                    </div>
                  </div>
                </div>
                            <div class="col-md-3 team-grids text-center">
                  <img src="https://beacon-link.com/wp-content/uploads/2019/03/client4.jpg" class="img-fluid" alt="" />
                  <div class="team-info">
                    <div class="caption">
                      <h4>BARTOW COUNTY JUVENILE COURT</h4>
                    </div>
                  </div>
                </div>
                            <div class="col-md-3 team-grids text-center">
                  <img src="https://beacon-link.com/wp-content/uploads/2019/03/Forsyth.jpg" class="img-fluid" alt="" />
                  <div class="team-info">
                    <div class="caption">
                      <h4>FORSYTH COUNTY, GA</h4>
                    </div>
                  </div>
                </div>
                            <div class="col-md-3 team-grids text-center">
                  <img src="https://beacon-link.com/wp-content/uploads/2019/03/DCOC.jpg" class="img-fluid" alt="" />
                  <div class="team-info">
                    <div class="caption">
                      <h4>DEKALB CHAMBER OF COMMERCE</h4>
                    </div>
                  </div>
                </div>
                            <div class="col-md-3 team-grids text-center">
                  <img src="https://beacon-link.com/wp-content/uploads/2019/03/HCOC.jpg" class="img-fluid" alt="" />
                  <div class="team-info">
                    <div class="caption">
                      <h4>GEORGIA HISPANIC CHAMBER OF COMMERCE</h4>
                    </div>
                  </div>
                </div>

                <ul class="bg-bubbles">
                      <li></li>
                      <li></li>
                      <li></li>
                      <li></li>
                      <li></li>
                      <li></li>
                      <li></li>
                      <li></li>
                      <li></li>
                      <li></li>
                    </ul>
                      </div>         
        </div>
      </div>

      </div>
      </div>
      

  <!-- Contact to Action Section -->

  
    <div class="container-fluid" id="findus">
      <div class="row">
          
        <!--Section Contac-->
        <div class="col-lg-6 main_grid_contact" data-sr-id="13" style="visibility: visible; opacity: 1; transform: matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transition: opacity 1s cubic-bezier(0.5, 0, 0, 1) 0s, transform 1s cubic-bezier(0.5, 0, 0, 1) 0s;">
          <div class="form-w3ls p-sm-5 p-4">
            <h4 class="mb-4 sec-title-w3 text-dark">Send us a message</h4>
            <div role="form" class="wpcf7" id="wpcf7-f146-o2" lang="en-US" dir="ltr">
      <div class="screen-reader-response"></div>
      <form id="formulario" action="/contacto.php" method="post" onsubmit="return comprobar()">
                <div class="nombre">
                    <input type="text" placeholder="Name" id="name" name="name" required>
                </div>
                <br>
                <div class="mail">
                    <input type="email" placeholder="Email" id="mail" name="mail" required>
                </div>
                <br>
                <div>
                    <textarea placeholder="Message" id="msg" name="msg" required></textarea>
                </div>
          
               <div class="" id="botonEnviar">  
                    <button type="submit" class="botonEnviar btn btn-primary btn-lg"><span style="">Send</span> </button>
                </div>
                  </form></div>						</div>
                      <div class="col-lg-6 banner-img" data-sr-id="0" style="visibility: visible; opacity: 1; transform: matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transition: opacity 1s cubic-bezier(0.5, 0, 0, 1) 0s, transform 1s cubic-bezier(0.5, 0, 0, 1) 0s;">
                      </div>
        </div>
        <!--Map-->
        <div class="col-lg-6 map" data-sr-id="12" style="">
                      
                        <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d6616.095519225373!2d-84.079863!3d33.991307!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x88f5bd6b058c73cd%3A0x4ca05994ceb0f08a!2s1755%20N%20Brown%20Rd%20%23200%2C%20Lawrenceville%2C%20GA%2030043%2C%20EE.%20UU.!5e0!3m2!1ses-419!2sve!4v1585166617850!5m2!1ses-419!2sve" width="400px" height="300px" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                         
        </div>
      <ul class="bg-bubbles">
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
      </ul>
    </div>
  



  <!-- Footer -->
  <footer class="footer py-5">
		
    <div class="container py-xl-4 py-lg-3">
  <div class="address row mb-4">
    <div class="col-lg-4 address-grid">
      <div class="row address-info">
        <div class="col-md-3 col-4 address-left text-lg-center text-sm-right text-center">
          <i class="fa fa-envelope"></i>
        </div>
        <div class="col-md-9 col-8 address-right">
          <p>
            <a href="mailto:info@beacon-link.com" class="text-light"> info@beacon-link.com</a>
          </p>
        </div>

      </div>
    </div>
    <div class="col-lg-4 address-grid my-lg-0 my-4">
      <div class="row address-info">
        <div class="col-md-3 col-4 address-left text-lg-center text-sm-right text-center">
          <i class="fa fa-phone"></i>
        </div>
        <div class="col-md-9 col-8 address-right">
          <p class="text-light">Language Department<br>
(470) 315-4949 EXT 301<br>
Media Department <br>
(470) 315-4949 EXT 302<br>
Toll-Free<br>
(844) 706-7388</p>
        </div>
      </div>
    </div>
    <div class="col-lg-4 address-grid">
      <div class="row address-info">
        <div class="col-md-3 col-4 address-left text-lg-center text-sm-right text-center">
          <i class="fa fa-map"></i>
        </div>
        <div class="col-md-9 col-8 address-right">
          <p class="text-light">1755 North Brown Road. Suite 200<br>
Lawrenceville, GA 30043</p>
        </div>
      </div>
    </div>
  </div>
  <!-- social icons footer -->
  <div class="w3l-footer text-center pt-lg-4 mt-5">
    <ul class="list-unstyled">
      <li>
        <a href="https://www.facebook.com/BeaconLink-266797260844967/" target="_blank">
          <img src="img/fb.png" alt="">
        </a>
      </li>
      <li class="mx-1">
        <a href="https://www.instagram.com/beaconlinkcomm/" target="_blank">
          <img src="img/ig.png" alt="">
        </a>
      </li>
      <li class="mx-1">
        <a href="https://www.youtube.com/channel/UCfGBUWRuLztitQ_ko27YmoA" target="_blank">
          <img src="img/you.png" alt="">
        </a>
      </li>
    </ul>
  </div>
  <!-- copyright -->
  <p style="text-align: right;" class="terms" ><a class="inline cboxElement" href="#inline_content">Terms and Conditions</a></p>
  <p style="text-align: left;" class="copy-right-grids text-light mt-4">© 2020 Beacon Link LLC. All Rights Reserved</p>
  <!-- //copyright -->
</div>
    
    
  
</footer>

  <!-- Bootstrap core JavaScript -->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Plugin JavaScript -->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
  <script src="vendor/magnific-popup/jquery.magnific-popup.min.js"></script>

  <!-- Custom scripts for this template -->
  <script src="js/creative.min.js"></script>

  <!--Hola-->
<!-- Js files -->
	<!-- JavaScript -->
	<!--<script src="https://beacon-link.com/wp-content/themes/beacon-theme/js/jquery-2.2.3.min.js"></script>-->
	<!-- Default-JavaScript-File -->
	<script src="https://beacon-link.com/wp-content/themes/beacon-theme/js/bootstrap.js"></script>
	<!-- Necessary-JavaScript-File-For-Bootstrap -->

	<!-- dropdown smooth -->
	
	<!-- //dropdown smooth -->

	<!-- stats -->
	<script src="https://beacon-link.com/wp-content/themes/beacon-theme/js/jquery.waypoints.min.js"></script>
	<script src="https://beacon-link.com/wp-content/themes/beacon-theme/js/jquery.countup.js"></script>
	
	
	<!-- //stats -->
  <!--fin dl hola-->

</body>

</html>
